/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gittesting;

/**
 *
 * @author agusl
 */
public class metodos {
    private final int numeros[] = {1,2,5,9};

    public metodos() {
    }
    
    public int mayorNumero(){
        int mayor = numeros[0];
        for (int i=1;i<numeros.length;i++){
            if (numeros[i]>mayor){
                mayor = numeros[i];
            }
        }
        return mayor;
    }
    
    public int menorNumero(){
        int menor = numeros[0];
        for (int i=1;i<numeros.length;i++){
            if (numeros[i]<menor){
                menor = numeros[i];
            }
        }
        return menor;
    }
    
}
